package com.BlackOut;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlackOutApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlackOutApplication.class, args);
	}

}
