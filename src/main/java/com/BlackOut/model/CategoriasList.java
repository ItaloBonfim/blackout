package com.BlackOut.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/list")
public class CategoriasList{
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Categorias> Listar() {
		
		Categorias cat1 = new Categorias(1, "Dev");
		Categorias cat2 = new Categorias(2, "Rh");
		
		List<Categorias> lista = new ArrayList<>();
		lista.add(cat1);
		lista.add(cat2);
		
		return lista;
	}
	

}
